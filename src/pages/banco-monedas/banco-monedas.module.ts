import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BancoMonedasPage } from './banco-monedas';

@NgModule({
  declarations: [
    BancoMonedasPage,
  ],
  imports: [
    IonicPageModule.forChild(BancoMonedasPage),
  ],
})
export class BancoMonedasPageModule {}
