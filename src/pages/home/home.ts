import { Component, ViewChild, ElementRef, Renderer2  } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { LoadingController, Loading } from 'ionic-angular';


declare var google;

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  public map;
  watch: any;
  public carMarkers;

  busqueda: String = '';

  loading: Loading;

  constructor(public loadingCtrl: LoadingController, 
              private geolocation: Geolocation, 
              public navCtrl: NavController, 
              public navParams: NavParams,
              private renderer: Renderer2) {
  }

  @ViewChild("buscar") buscarElement: ElementRef;
  @ViewChild("recomendaciones") recomendaciones: ElementRef;
  @ViewChild("localizacion") localizacion: ElementRef;
  @ViewChild("rowDown") rowDown: ElementRef;
  @ViewChild("rowUp") rowUp: ElementRef;

  @ViewChild(Content) content: Content;


  addClass(elem, clase){
    this.renderer.addClass(elem.nativeElement, clase);
  }

  removeClass(elem,clase){
    this.renderer.removeClass(elem.nativeElement,clase);
  }

  scrollToTop() {

    this.addClass(this.recomendaciones,"oculto");
    this.removeClass(this.rowDown,"oculto");
    this.addClass(this.rowUp,"oculto");
    this.removeClass(this.localizacion,"oculto");
    this.content.scrollToTop();
  }

  scrollToBottom() {

    this.removeClass(this.recomendaciones,"oculto");
    this.removeClass(this.rowUp,"oculto");
    this.addClass(this.rowDown,"oculto");
    this.addClass(this.localizacion,"oculto");

    this.content.scrollToBottom();
    
  }

  blurBuscar() {
    this.busqueda = '';
  }

  ionViewDidLoad() {

    this.carMarkers = [];

    this.loading = this.loadingCtrl.create({
      content: "Cargando..."
    });

    this.loading.present();

    this.geolocation.getCurrentPosition().then(pos => {
      this.map = this.createMap(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      this.agregarMarcador(pos);
      this.loading.dismiss();
    }).catch((err) => {
      console.log(err);
    })

    this.watch = this.geolocation.watchPosition().subscribe((pos) => {
      console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
      this.carMarkers[0].setPosition(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
    });

  }
  ionViewWillLeave() {
    console.log("unsubscribe");
    this.watch.unsubscribe();
  }

  createMap(location) {
    let mapOptions = {
      center: location,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }

    let mapElement = document.getElementById('map');
    let map = new google.maps.Map(mapElement, mapOptions);

    return map;
  }

  cargarLocalizacion() {
    this.loading = this.loadingCtrl.create({
      content: "Cargando..."
    });
    this.loading.present();

    this.geolocation.getCurrentPosition().then(pos => {
      this.carMarkers[0].setPosition(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      this.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      this.loading.dismiss();
    }).catch((err) => {
      console.log(err);
    })
  }

  agregarMarcador(pos) {
    var imagen = 'https://image.ibb.co/b2KxWo/ping_3.png';

    let carMarker = new google.maps.Marker({
      map: this.map,
      position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude)
      , icon: imagen
    });

    carMarker.set('id', 1);
    this.carMarkers.push(carMarker);
  }

}
