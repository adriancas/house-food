import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pueblos-magicos',
  templateUrl: 'pueblos-magicos.html',
})
export class PueblosMagicosPage {

  screen: any = {width: 0, height: 0};

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.screen = JSON.parse(window.localStorage.getItem('screen'));
    console.log(this.screen);
  }

  ionViewWillEnter() {
   
  }



}
