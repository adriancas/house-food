import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PueblosMagicosPage } from './pueblos-magicos';

@NgModule({
  declarations: [
    PueblosMagicosPage,
  ],
  imports: [
    IonicPageModule.forChild(PueblosMagicosPage),
  ],
})
export class PueblosMagicosPageModule {}
