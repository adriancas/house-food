import { Component, ViewChild } from '@angular/core';
import { Nav, IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-container',
  templateUrl: 'container.html',
})
export class ContainerPage {

  @ViewChild(Nav) nav: Nav; 

  rootPage: any = "HomePage";
  activePage: any;
  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.pages = [
      { title: "Inicio", component: "HomePage", icon: 'home' },
      { title: "Método de pago", component: "MetodoPagoPage", icon: 'card' },
      { title: "Invitar amigos", component: "InvitarAmigosPage", icon: 'chatbubbles' },
      { title: "Banco de monedas", component: "BancoMonedasPage", icon: 'basket' },
      { title: "Ayuda", component: "AyudaPage", icon: 'help-circle' },
    ];

    this.activePage = this.pages[0];

  }

  openPage(page) {
    this.nav.setRoot(page.component);
    this.activePage = page;
  }

  menuActive(page) {
    return page == this.activePage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContainerPage');
  }

  goPerfil(){
    this.navCtrl.push("PerfilPage");
  }

}
