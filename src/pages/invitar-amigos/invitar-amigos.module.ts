import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvitarAmigosPage } from './invitar-amigos';

@NgModule({
  declarations: [
    InvitarAmigosPage,
  ],
  imports: [
    IonicPageModule.forChild(InvitarAmigosPage),
  ],
})
export class InvitarAmigosPageModule {}
