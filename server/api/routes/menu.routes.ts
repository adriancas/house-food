var router = require('express').Router();
var MenuController = require('../controllers/menus.controller');

	//Save Pais
	router.post('/',MenuController.saveMenu);

	//Update Pais
	router.put('/:idMenu',MenuController.updateMenu);

	//Delete Pais
	router.delete('/:idMenu',MenuController.deleteMenu);

	//Get Pais
	router.get('/:idMenu', MenuController.getMenu);

	//List Pais
	router.get('/', MenuController.ListMenu);

module.exports = router;