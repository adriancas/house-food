var router = require('express').Router();
var pueblosMagicos=require('../controllers/pueblos_magicos.controller');
//save
router.post('/',pueblosMagicos.savePueblosMagicos);
//list
router.get('/',pueblosMagicos.listPueblosMagicos);
//update
router.put('/:idPueblos',pueblosMagicos.updatePueblosMagicos);
//delete
router.delete('/:idPueblos',pueblosMagicos.deletePueblosMagicos);

module.exports=router;