var router = require('express').Router();
var PaisController = require('../controllers/paises.controller');

	//Save Pais
	router.post('/',PaisController.savePais);

	//Update Pais
	router.put('/:idPais',PaisController.updatePais);

	//Delete Pais
	router.delete('/:idPais',PaisController.deletePais);

	//Get Pais
	router.get('/:idPais', PaisController.getPais);

	//List Pais
	router.get('/', PaisController.ListPais);

module.exports = router;