var router = require('express').Router();
var CategoriasController = require('../controllers/categorias.controller');
//Save Proveedor
router.post('/', CategoriasController.saveCategoria);

//Update Proveedor
router.put('/:idCategoria', CategoriasController.updateCategoria);

//FindAll Artist
router.get('/', CategoriasController.listCategorias);

//Delete Artist
router.delete('/:idCategoria', CategoriasController.deleteCategoria);


module.exports = router;