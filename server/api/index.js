var router = require('express').Router();

router.use('/categorias', require('./routes/categorias.routes'));
router.use('/pais',require('./routes/paises.routes'));
router.use('/menu',require('./routes/menu.routes'));
module.exports = router;