const Pueblos_magicos=require('../../model/pueblos_magicos');

async function listPueblosMagicos(req,res){
    try{
        var pueblosMagicos = await Pueblos_magicos.find();
        res.json(pueblosMagicos);

    }catch(err){res.status(500).send({message:err})}
}


async function savePueblosMagicos(req,res){    
    try{
        var pueblosMagicos= new Pueblos_magicos({
            nombre: {type:String},
            imagen: {type:String},
            descripcion: {type:String}
        });
        var saved= await pueblosMagicos.save();
        res.json(saved);

    }catch(err){res.status(500).send({message:err});}

}

async function updatePueblosMagicos(req,res){
    try{
        var id=req.params.idPueblos;
        var pueblosMagicos=new Pueblos_magicos({
            nombre: {type:String},
            imagen: {type:String},
            descripcion: {type:String}
        });
        var modified= await Pueblos_magicos.findByIdAndUpdate(id,pueblosMagicos);
        res.json(modified);
    }catch(err){res.status(500).send({message:err});}

}

async function deletePueblosMagicos(req,res){
    try{
        var id=req.params.idPueblos;
        var deleted= await Pueblos_magicos.findByIdAndRemove(id);
        res.json(deleted);
    }catch(err){res.status(500).json(err);}
}

module.exports={
    listPueblosMagicos,
    savePueblosMagicos,
    updatePueblosMagicos,
    deletePueblosMagicos
}