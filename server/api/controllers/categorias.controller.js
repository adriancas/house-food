var Categoria = require('../../model/categorias.model');
var fse = require('fs-extra');
var multiparty = require('multiparty');

async function listCategorias(req, res) {
    try {
        var categorias = await Categoria.find();
        res.json(categorias);
    } catch (err) {
        res.status(500).json(err)
    }
}

function saveCategoria(req, res) {
    var form = new multiparty.Form();
    var categoria = new Categoria();
    try {
        form.parse(req, function (err, fields, files) {
            if (files.imagen[0].originalFilename != '') {
                fse.copy(files.imagen[0].path, path.join(__dirname, '../../public/images/categorias/' + files.imagen[0].originalFilename))
                    .then(() => {
                        categoria.nombre = fields.nombre,
                            categoria.imagen = files.imagen[0].originalFilename,
                            categoria.descripcion = fields.descripcion
                        return categoria.save();
                    }).then((categoria) => {
                      return  res.json(categoria)
                    })
            } else {
                return res.status(500).json(err)
            }
        })
    } catch (err) {
        res.status(500).json(err);
    }
}

function updateCategoria(req, res) {
    var id = req.params.idCategoria;
    var form = new multiparty.Form();
    var categoria = new Categoria();
    try {
        if (files.imagen[0].originalFilename != '') {
            fse.copy(files.imagen[0].path, path.join(__dirname, '../../public/images/categorias/' + files.imagen[0].originalFilename))
                .then(() => {
                    return Categoria.findById(id);
                }).then((categoria) => {
                    return fse.unlink(path.join(__dirname, '../../public/images/categorias/' + categoria.imagen))
                })
                .then(() => {
                    categoria._id = id,
                        categoria.nombre = fields.nombre,
                        categoria.imagen = files.imagen[0].originalFilename,
                        categoria.descripcion = fields.descripcion
                  return Categoria.findByIdAndUpdate(id, categoria);
                }).then((categoria) => {
                  return res.json(categoria)
                })
        } else {
            categoria._id = id,
                categoria.nombre = fields.nombre,
                categoria.descripcion = fields.descripcion
           return Categoria.findByIdAndUpdate(id, categoria)
                .then((categoria) => {
                   return res.json(categoria)
                })
        }
    } catch (err) {
        res.status(500).json(err);
    }
}

async function deleteCategoria(req, res) {
    try {
        var id = req.params.idCategoria;
        var deleted = await Categoria.findByIdAndRemove(id)
        res.json(deleted);
    } catch (err) {
        res.status(500).json(err);
    }
}

module.exports = {
    saveCategoria,
    updateCategoria,
    deleteCategoria,
    listCategorias
}
