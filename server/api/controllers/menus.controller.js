var Menu = require('../../model/menus.model');
var fse = require('fs-extra');
var multiparty = require('multiparty');
var path = require('path');

 function saveMenu(req,res){

	var menu = new Menu();
	var form = new multiparty.Form();

	try {
        form.parse(req, function (err, fields, files) {
            if (files.imagen[0].originalFilename != '') {
                fse.copy(files.imagen[0].path, path.join(__dirname, '../../public/images/menu/' + files.imagen[0].originalFilename))
                    .then(() => {

                        	menu.nombre = fields.nombre;
                            menu.imagen = files.imagen[0].originalFilename;
                            menu.descripcion = fields.descripcion;
                            menu.horas.horaInicio = fields.horaInicio;
                            menu.horas.horaFin = fields.horaFin;
                            menu.categoria = fields.categoria;
                            menu.disponibilidad = fields.disponibilidad;

                        return menu.save();

                    }).then((menu) => {

                      return  res.json(menu);

                    })

            } else {
                return res.status(500).json(err)
            }
        })
    } catch (err) {
        res.status(500).json(err);
    }


   
} 

 function updateMenu(req,res){

	var menu = new Menu();
	var form = new multiparty.Form();
	var id = req.params.idMenu;

	try{

		

		form.parse(req, function (err, fields, files) {
                if (files.imagen[0].originalFilename != '') {

                    fse.copy(files.imagen[0].path, path.join(__dirname, '../../public/images/menu/' + files.imagen[0].originalFilename))
                        .then(() => {
                            return Menu.findById(id);
                        }).then((menu) => {
                            return fse.unlink(path.join(__dirname, '../../public/images/menu/' + menu.imagen))
                        })
                        .then(() => {

                            menu.nombre = fields.nombre;
                            menu.imagen = files.imagen[0].originalFilename;
                            menu.descripcion = fields.descripcion;
                            menu.horas.horaInicio = fields.horaInicio;
                            menu.horas.horaFin = fields.horaFin;
                            menu.categoria = fields.categoria;
                            menu.disponibilidad = fields.disponibilidad;

                          return Menu.findByIdAndUpdate(id, menu);

                        }).then((menu) => {
                          return res.json(menu)
                        })

                }else{

                    menu.nombre = fields.nombre;
                    menu.descripcion = fields.descripcion;
                    menu.horas.horaInicio = fields.horaInicio;
                    menu.horas.horaFin = fields.horaFin;
                    menu.categoria = fields.categoria;
                    menu.disponibilidad = fields.disponibilidad;

                    Menu.findByIdAndUpdate(id, menu)
                        .then((menu) => {
                            res.json(menu);
                        }).catch((err)=>{
                            res.status(500).json(err);
                        })
                }
		})


	}catch(err){
		res.status(500).json(err);
	}


}

async function deleteMenu(req,res){

	var id = req.params.idMenu;

	try{

		var deleted = await Menu.findByIdAndRemove(id);
        res.json(deleted);
	
	} catch (err) {
        res.status(500).json(err);
    }

}

async function getMenu(req,res){

	var id = req.params.idMenu;


	try{
		var menu = Menu.findById(id);
		res.json(pais);
	}catch(err){
		res.status(500).json(err);
	}

}

async function ListMenu(req,res){

	try {
        var menus = await Menu.find();
        res.json(menus);
    } catch (err) {
        res.status(500).json(err)
    }

}


module.exports = {
	saveMenu,
	updateMenu,
	deleteMenu,
	getMenu,
	ListMenu
}