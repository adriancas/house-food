var Paises = require('../../model/paises.model');
var fse = require('fs-extra');
var path = require('path');
var multiparty = require('multiparty');

 function savePais(req,res){

	var pais = new Paises();
	var form = new multiparty.Form();

	try {
        form.parse(req, function (err, fields, files) {
            if (files.imagen[0].originalFilename != '') {
                fse.copy(files.imagen[0].path, path.join(__dirname, '../../public/images/paises/' + files.imagen[0].originalFilename))
                    .then(() => {

                        	pais.nombre = fields.nombre;
                            pais.imagen = files.imagen[0].originalFilename;
                            pais.descripcion = fields.descripcion;
                            pais.moneda = fields.moneda;
                            pais.idioma = fields.idioma;

                        return pais.save();

                    }).then((pais) => {

                      return  res.json(pais);

                    })

            } else {
                return res.status(500).json(err)
            }
        })
    } catch (err) {
        res.status(500).json(err);
    }


   
} 

 function updatePais(req,res){

	var pais = new Paises();
	var form = new multiparty.Form();
	var id = req.params.idPais;

	try{

		

		form.parse(req, function (err, fields, files) {

			if (files.imagen[0].originalFilename != '') {

				fse.copy(files.imagen[0].path, path.join(__dirname, '../../public/images/paises/' + files.imagen[0].originalFilename))
	                .then(() => {
	                    return Paises.findById(id);
	                }).then((pais) => {
	                    return fse.unlink(path.join(__dirname, '../../public/images/paises/' + pais.imagen))
	                })
	                .then(() => {
	                    pais.nombre = fields.nombre;
	                    pais.imagen = files.imagen[0].originalFilename;
	                    pais.descripcion = fields.descripcion;
	                    pais.moneda = fields.moneda;
	                    pais.idioma = fields.idioma;

	                  return Paises.findByIdAndUpdate(id, pais);

	                }).then((pais) => {
	                  return res.json(pais)
	                })

			}else{

				pais.nombre = fields.nombre;
	            pais.descripcion = fields.descripcion;
	            pais.moneda = fields.moneda;
	            pais.idioma = fields.idioma;

	            Paises.findByIdAndUpdate(id, pais)
	                .then((pais) => {
	                    res.json(pais);
	                }).catch((err)=>{
	                	res.status(500).json(err);
	                })
			}
			
		})


	}catch(err){
		res.status(500).json(err);
	}


}

async function deletePais(req,res){

	var id = req.params.idPais;

	try{

		var deleted = await Paises.findByIdAndRemove(id);
        res.json(deleted);
	
	} catch (err) {
        res.status(500).json(err);
    }

}

async function getPais(req,res){

	var id = req.params.idPais;
	console.log("id==>" + id);

	try{
		var pais = await Paises.findById(id);
		res.json(pais);
	}catch(err){
		res.status(500).json(err);
	}

}

async function ListPais(req,res){

	try {
        var paises = await Paises.find();
        res.json(paises);
    } catch (err) {
        res.status(500).json(err)
    }

}


module.exports = {
	savePais,
	updatePais,
	deletePais,
	getPais,
	ListPais
}