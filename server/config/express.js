var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var DB_CONFIG = require('./database.config');

mongoose.connect(`${DB_CONFIG.URL}:${DB_CONFIG.PORT}/${DB_CONFIG.DATABASE}`, {useNewUrlParser: true});

var app = express();

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//app.use('/api', require('../api/api'));
app.use('/', require('../api/index'));

module.exports = app;