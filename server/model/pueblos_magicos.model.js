var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var pueblos_magicos_schema = new Schema({
    nombre: {type:String},
    imagen: {type:String},
    descripcion: {type:String}
});

module.exports = mongoose.model("PueblosMagicos", pueblos_magicos_schema);