var especificaciones_schema = new Schema({
    descripcion: { type: String },
    tipo: { type: String },
    status: { type: Boolean }
});

module.exports = especificaciones_schema;