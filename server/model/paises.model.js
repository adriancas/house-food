var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var paises_schema = new Schema({
    nombre: {type:String},
    imagen: {type:String},
    descripcion: {type:String},
    moneda: {type:String},
    idioma: {type:String}
});

module.exports = mongoose.model("Paises", paises_schema);