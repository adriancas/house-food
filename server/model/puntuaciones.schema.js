var puntuaciones_schema = new Schema({
    puntuacion: { type: Number },
    comentario: { type: String },
    fecha: { type: Date }
});

module.exports = puntuaciones_schema;