var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var categorias_schema = new Schema({
    nombre: {type:String},
    imagen: {type:String},
    descripcion: {type:String}
});

module.exports = mongoose.model("Categorias", categorias_schema);