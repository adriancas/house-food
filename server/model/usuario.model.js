var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var puntuaciones_schema = "./puntuaciones_schema";
var especificaciones_schema = "./especificaciones_schema";

var ubicacion_schema = new Schema({
    lat: { type: String },
    long: { type: String }
});

var usuarios_schema = new Schema({
    usuario: {type:String},
    password: {type:String},
    nombre: {type:String},
    foto: {type:String},
    correo: {type:String},
    ubicacion: ubicacion_schema,
    puntuaciones: [puntuaciones_schema],
    descripcion: {type:String},
    precio: {trpe:Number},
    menus: [{ type: Schema.Types.ObjectId, ref: "Menus" }],
    especificaciones: [especificaciones_schema]
});

module.exports = mongoose.model("Usuarios", usuarios_schema);