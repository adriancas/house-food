var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var horas_schema = new Schema({
    horaInicio: { type: Date },
    horaFin: { type: Date }
});

var menus_schema = new Schema({
    nombre: {type:String},
    imagen: {type:String},
    descripcion: {type:String},
    horas: horas_schema,
    categoria:  { type: Schema.Types.ObjectId, ref: "Categorias" },
    disponibilidad: {type:Boolean}
});

module.exports = mongoose.model("Menus", menus_schema);